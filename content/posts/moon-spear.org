#+title: The Moon Spear - A Fudge Fantasy adventure
#+date: 2020-06-01T18:51:04+02:00
#+tags[]: fudge rpg adventure
#+draft: false

* Introduction
#+BEGIN_aside
This is my third Fudge Compendium article. This adventure is based on a dream I had quite a
while ago. When I woke up I promptly made enough notes to write his adventure.

In the dream I was trying to find a spear called the Moon Spear. It was some sort of “trust
in god type of thing” and one of the obstacles was to select the correct spear among
multiple ones. I think I was inspired by some movies. If the spear was picked correctly
some sort of hidden place would open up. The hidden place contained a cloth bundle
containing several items. To make things harder, only a child could pick the correct spear
in moonlight. Once the correct choice was made, some prayer resulted in the spear
appearing. With the spear comes the shadow guard, an army of fierce spirit warriors, only
visible in the moonlight. This is also clearly inspired by books and movies.

Meanwhile some bad dude had come before, but had failed to identify the correct spear, and
left with what he thought was the moon spear. This is when I woke up. In order to create
functioning adventure, I had to add things not in the dream. Remember it was a dream, so
you should feel free to make some changes for it to fit your world.

I have also on purpose not included any maps or charts but instead verbal descriptions of
places in order to make it easier for you to insert this into your game world.
#+END_aside
# more

* Background
The adventure takes place around three kingdoms of the southern coast: Ard, Kesadh and
South Dahler. There is a North Dahler somewhere in the north, but it’s not relevant to this
adventure. These three kingdoms have existed more or less in peace for the last 10
years. Recently the King of South Dahler has decided that he’d like to annex Ard, since it
has good mineral deposits and a well protected mountainous coast.

The South Dahlers are known for their large and fierce army. However the Ard are known for
their strong wizards of the past who are said to have made sure that Ard would never
fall. So for this the South Dahlers have now been mustering their army, and are massing it
on the border of Ard.

Ard is a poor but peaceful kingdom, with mountainous pasture land. There is only one city,
Ard, located atop of a mountain in the center of the country. To the south it borders with
the South Sea, and to the east and north lies South Dahler. Kesadh covers the short western
border.

One of the ancient defenses of the kingdom is the fabled Moon Spear, which is said to wield
the power of armies of the Moon. The king of Ard, Ardur IX, is now looking for brave
adventurers to help him defend his kingdom.

* The task
The task is quite straightforward yet hard. The task is to retrieve the fabled Moon Spear
from its hiding place.

The party will first need to locate the Moon Caves, where the spear supposedly rests. The
spear is said to be guarded by the Moon Goddess, Ar. One needs to be brave and genuinely
honest in order to succeed.  The most kept secret of the kingdom is that in order to
succeed one needs to have the innocence of a child, but yet the courage of a lion. For
this, the king’s granddaughter, Ariadne, is kept protected in a secret location. She has
been brought up to be brave in the face of danger, and yet be innocent about the true
nature of man. She has no understanding of evil at all, and won’t be able to distinguish
friend from foe. Also since she is brave she has a tendency to overlook danger to herself.

The secondary task is to keep Ariadne safe from corruption in order for the mission to be
successful. She is also not to come to any harm (naturally).

* The Moon Cave
The Moon cave is located somewhere on the south coast on “a yellow beach, where the moon
shines into the cave”. The site is indeed located on the south shore. Most of the beach is
covered with gray or brown sand, but a small cove in the middle of the coast is actually
yellow sand. The closer to the cove one comes the more exact directions one can get from
the locals. Characters can roll for Tracking, Region Lore or Scrounging or similar skills
to get clues on where to head. At first these rolls should be easy if only looking for the
generic area to head towards, and will get harder as they approach the exact location.

One further problem is the fact that the cove is pockmarked with caves. However, only one
of them carries a shrine to the moon. It is quite easily distinguishable from the other
caves, when looked more closely.

The cave itself is very rugged and simple. At first it will look to be a single cave with
some nice wooden screens on the back wall. Upon further examination (Fair or better
perception) there is a second chamber behind the screen. The second chamber contains the
actual moon shrine with over a dozen (14) spears strewn all around the room. Some more
magnificent than others. Most of these spears have been donated to the shrine by old
warlords, and some are quite fine craftsmanship. Behind the shrine is what seems to be a
panel, but is actually a wooden screen. Few realize (Superb or better perception) the true
nature of the screen.

The innermost chamber opens up when the correct spear is selected in the altar room (it’s a
simple trigger). Inside the sanctum there are 3 altars covered with red cloth. Only persons
who are pure of heart and strong of mind can enter. This can be handled by a series of Will
or Courage rolls in combination with Empathy or Insight rolls. I Suggest a Rolled Degree of
Superb on both rolls. People who fail the Courage test will flee the chamber and not stop
until they drop from exhaustion or are otherwise calmed down. People who fail the Empathy
roll will be overcome with bliss and stand dazzled until they collapse from exhaustion or
are dragged out of the temple.

The altars contain 3 stone tablets that contain a spell that will summon the Moon
Spear. Naturally the spell may only be read in moonlight. Once the spear is summoned the
wielder of the spear can summon the Shadow Guard and command them. The spear is a +4 ODF
weapon that can shoot Moonlight beams which are +3 ODF ranged weapons, however, the spear
only works against enemies of Ard, and should it be misused it will shatter in a flash of
light.

* Obstacles
The King of South Dahler has managed to find out about the moon spear, and decides to get
it for himself. The king has dispatched his right-hand man, Lord Ipecac, to retrieve the
spear or to destroy it, for he fears it.

Ipecac has managed to bribe and intimidate his way to the approximate whereabouts of the
Moon Cave. He is unaware of the secret requirement of the pure innocence. He is ruthless
and clever, and will stop at nothing to reach his goals. He rides with four of his most
elite warriors, who serve as his honor guard. They are as cruel as he is.

* Aids
The party is hired by the court, and the characters will have more or less the entire
resources of the kingdom at their disposal. It is however a very poor kingdom, and most of
the characters should be from Ard, or have family living there. Thus they should not want
to demand an extravagant price, but should rather be motivated to simply save their home,
and way of living. This can be played as a starting adventure in a campaign.

The king will give them free access to the royal library, to search for clues. In the
library there is a tome which describes the location by word, but doesn’t have a map. The
librarian is the one who carries the secret knowledge of the requirement of the
innocent. He has informed the king, and they will make a secret (even to the characters)
rendezvous on the day the adventurers leave the city.

* Encounters
When traveling to the beach the characters may run into scouting parties of the advancing
armies. There are of course other dangers when traveling over mountains, like random
monsters and weather.

There should be at least one encounter with Ipecac and his guard. Probably Ipecac will
easily defeat the characters unless they are clever. They will die for sure, should they
challenge him directly.

Having Ipecac re-appear should make it clear to the characters, who is the bad guy. Perhaps
Ipecac will defeat the characters and steal their “map”, or perhaps he will kidnap the
child out of spite, or could he know she is important?

Once the characters find the correct beach and the correct cave (the beach is limestone, so
contains quite a few caves. But most of them are empty, but they can be quite deep. It
shouldn’t be hard to find the cave since all locals know about the shrine to the moon
goddess. That is if the PCs talk to the locals and if they mention the moon. Otherwise the
locals will keep to their own business. If Ipecac manages to capture the girl, the
characters may want to ask if they have seen him, to which they will get an affirmative
reply.

** Suggested encounters
*** Wolves
During the night whoever is on guard can hear wolves howling close by. They should roll a
perception against Ariadne’s Stealth. If the succeed they will notice her sneaking out of
her tent intent on investigating the howling song. She will try to outrun the guard if they
try to stop her. Running straight for the wolf howling.

If the guard fails to notice Ariadne sneaking away, her absence will probably go unnoticed
until the next shift change. It should be relatively easy to track her. They will notice
her heading towards where the wolves were. Once they find her, she will most likely be
surrounded by wolves, which she will adore. The wolves will act almost puppy-like around
her, but will not like the intrusion of the party. They will most likely try to defend her,
if the party acts angry or aggressively.

*** Giants
Giants roam the mountains of Ard. Usually they eat mountain goats, but occasionally they
will snack on humans if they can catch them unawares. This could again be an encounter
created by Ariadne sneaking off when the party takes a break. Ariadne won’t be afraid of
the Giant(s). But how can the party handle the encounter without upsetting Ariadne’s
innocence?

*** Bandits
Banditry is rare in Ard (the farmers don’t really have any money), but bandits of South
Dahler sometimes flee into Ard, to escape the cruel reeves of South Dahler.
* Characters
Instead of providing pre-generated characters it is suggested that you use [[https://www.drivethrurpg.com/product/95384/Fudge-On-The-Fly][Fudge on the Fly]]
for character generation.

If you need an easy Character Record Sheet for that, you can download [[https://drive.google.com/file/d/1tpXLsfGvpPjFPnCHGnHjXCTimuu_8njP/view?usp=sharing][this PDF]] I made.

* NPCs
** Lord Ipecac
#+HTML: <div class="bigtable">
| Value    | Skills                                 |
|----------+----------------------------------------|
| Superb   | Fighting                               |
| Great    | Tactics, Logic                         |
| Good     | Will, Magic, Perception                |
| Fair     | Athletics, Literature, Poetry, History |
| Mediocre | Empathy, Stealth, Climbing             |
| Poor     | All other skills                       |

| *ODF* | +3 with sword | +4 with Magic (can cast a lightning bolt every other turn) |
| *DDF* | +2 with armor |                                                            |
#+HTML: </div>

Lord Ipecac is evil and cruel. He is a very skilled swordsman and a competent sorcerer. He
loves to give cause pain and suffering, especially to his opponents. Lord Ipecac is at the
same time fiercely loyal to his King.

** Ipecac’s guard
#+HTML: <div class="bigtable">
| Value | Skills                         |
|-------+--------------------------------|
| Great | Fighting, Riding               |
| Good  | Perception, Athletics, Tactics |
| Fair  | Climbing, Stealth              |
| Poor  | All other skills               |

| *ODF* | +4 With Lance | +3 With sword | +3 with Bow |
| *DDF* | +3 With armor |               |             |
#+HTML: </div>

Ipecac’s Guard are Knights of South Dahler wearing Plate armor and very skilled fighters.

** Ariadne princess of Ard
| Value    | Skills            |
|----------+-------------------|
| Great    | Will, Empathy     |
| Good     | Perception        |
| Fair     | Riding            |
| Mediocre | Climbing, Stealth |
| Poor     | All other skills  |

- Fault: Curious and innocent :: tends to go off on own to explore the world around her,
  without understanding the dangers.
- Gift: Brave :: +1 to Will roll to resist scary things.

Ariadne is a 12 year old girl, and as said is totally innocent about the way of the
world. She simply can’t comprehend anyone hurting anyone else. Every time she is confronted
with evil things such as killing or hurting people or animals, roll 4dF. On a negative
result she becomes more jaded and her Empathy goes Down. When her Empathy reaches Fair, she
is no longer innocent, and cannot easily pick the right spear, nor pass to the innermost
chamber.
